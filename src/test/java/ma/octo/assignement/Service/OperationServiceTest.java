package ma.octo.assignement.Service;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.VersementServiceImpl;
import ma.octo.assignement.service.VirementServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class OperationServiceTest {

    @Autowired
    VirementServiceImpl virementService;
    @Autowired
    VersementServiceImpl versementService;
    @Autowired
    CompteRepository compteRepository;
    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Test
    public void SoldeBeneficiaireVirement() throws Exception{

        Compte compteBeneficiaireAvantVirement = compteRepository.findByNrCompte("010000B025001000");
        BigDecimal soldeAvant = compteBeneficiaireAvantVirement.getSolde();

        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur("010000A000001000");
        virementDto.setDate(new Date());
        virementDto.setMotif("Assignment 2021");
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setNrCompteBeneficiaire("010000B025001000");
        virementService.createTransaction(virementDto);

        Compte compteBeneficiaireApresVirement = compteRepository.findByNrCompte("010000B025001000");
        BigDecimal soldeApres = compteBeneficiaireApresVirement.getSolde();

        Assertions.assertEquals(soldeAvant.add(new BigDecimal(100.00)).floatValue(),soldeApres.floatValue());
    }

    @Test
    public void SoldeEmetteurVirement() throws Exception{

        Compte compteEmetteurAvantVirement = compteRepository.findByNrCompte("010000A000001000");
        BigDecimal soldeAvant = compteEmetteurAvantVirement.getSolde();

        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur("010000A000001000");
        virementDto.setDate(new Date());
        virementDto.setMotif("Assignment 2021");
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setNrCompteBeneficiaire("010000B025001000");
        virementService.createTransaction(virementDto);

        Compte compteEmetteurApresVirement = compteRepository.findByNrCompte("010000A000001000");
        BigDecimal soldeApres = compteEmetteurApresVirement.getSolde();

        Assertions.assertEquals(soldeAvant.subtract(new BigDecimal(100.00)).floatValue(),soldeApres.floatValue());
    }


    @Test
    public void SoldeBeneficiaireVersement() throws Exception{

        Compte compteBeneficiaireAvantVersement = compteRepository.findByNrCompte("010000B025001000");
        BigDecimal soldeAvant = compteBeneficiaireAvantVersement.getSolde();

        VersementDto versementDto = new VersementDto();
        versementDto.setNomPrenomEmetteur("sender");
        versementDto.setDate(new Date());
        versementDto.setMotif("Assignment 2021");
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setNrCompteBeneficiaire("010000B025001000");
        versementService.createTransaction(versementDto);

        Compte compteBeneficiaireApresVersement = compteRepository.findByNrCompte("010000B025001000");
        BigDecimal soldeApres = compteBeneficiaireApresVersement.getSolde();

        Assertions.assertEquals(soldeAvant.add(new BigDecimal(100.00)).floatValue(),soldeApres.floatValue());

    }

}
