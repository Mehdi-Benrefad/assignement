package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVersementService {

    List<VersementDto> loadAll();
    void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;

}
