package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class VersementServiceImpl implements IVersementService{

    public static final int MONTANT_MAXIMAL = 10000;

    @Autowired
    VersementRepository versementRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditServiceImpl auditService;

    @Override
    public List<VersementDto> loadAll() {
        List<Versement> all = versementRepository.findAll();
        List<VersementDto> all_bis = new ArrayList<>();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            for(int i = 0 ; i< all.size() ; i++){
                all_bis.add(VersementMapper.map(all.get(i)));
            }
            return all_bis;
        }
    }

    @Override
    public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {

        String nom_prenom_emetteur = versementDto.getNomPrenomEmetteur();
        Compte compteRecepteur = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());

        if (compteRecepteur == null) {
            System.out.println("Compte Recepteur Non existant");
            throw new CompteNonExistantException("Compte Recepteur Non existant");
        }

        if (versementDto.getMontantVersement()==null) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant nul");
            throw new TransactionException("Montant nul");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (versementDto.getMotif() == null) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compteRecepteur.setSolde(new BigDecimal(compteRecepteur.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(compteRecepteur);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(compteRecepteur);
        versement.setNomPrenomEmetteur(nom_prenom_emetteur);
        versement.setMontant(versementDto.getMontantVersement());
        versementRepository.save(versement);

        auditService.auditVersement("Versement de la part de " + versementDto.getNomPrenomEmetteur() + " vers " + versementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }
}
