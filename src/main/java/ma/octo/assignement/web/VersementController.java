package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IVersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController(value = "/versements")
public class VersementController {
    @Autowired
    IVersementService versementService;

    @GetMapping("lister_versements")
    List<VersementDto> loadAll() {
        return versementService.loadAll();
    }

    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createTransaction(@RequestBody VersementDto versementDto) {
        Map<String , String> response = new HashMap();

        try {
            versementService.createTransaction(versementDto);
        }catch(CompteNonExistantException e){
            response.put("error",e.toString().split(":")[1]);
        }
        catch(TransactionException e){
            response.put("error",e.toString().split(":")[1]);
        }

        if(response.isEmpty()){
            response.put("success" , "Versement valide");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);

    }
}
