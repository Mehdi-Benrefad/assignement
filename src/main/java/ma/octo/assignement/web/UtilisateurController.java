package ma.octo.assignement.web;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/utilisateurs")
public class UtilisateurController {
    @Autowired
    IUtilisateurService utilisateurService;

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return utilisateurService.loadAllUtilisateur();
    }
}
