package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/comptes")
public class CompteController {

    @Autowired
    ICompteService compteService;

    @GetMapping("lister_comptes")
    List<Compte> loadAllCompte() {
        return compteService.loadAllCompte();
    }
}
