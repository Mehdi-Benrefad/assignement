package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter @Setter
public class VersementDto {

        private String nomPrenomEmetteur;
        private String nrCompteBeneficiaire;
        private String motif;
        private BigDecimal montantVersement;
        private Date date;

}
