package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
public class AuditVersement extends Audit {
  ////il est utilise pour cerialiser ou decerialiser un objet
  public static final long serialVersionUID = 1L;

}
