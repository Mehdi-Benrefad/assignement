package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "OPERATION")
@Getter @Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Operation implements Serializable {
    //Cette Classe peut etre transmise et instanciee dans une autre machine
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal montant;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;

    @Column(length = 200)
    private String motif;

    @ManyToOne
    private Compte compteBeneficiaire;

}
