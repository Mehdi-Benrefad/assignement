package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;
import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@Table(name = "AUDIT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Audit implements Serializable {
    //Cette Classe peut etre transmise et instanciee dans une autre machine
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 100)
    private String message;

    @Enumerated(EnumType.STRING)
    private EventType eventType;
}
