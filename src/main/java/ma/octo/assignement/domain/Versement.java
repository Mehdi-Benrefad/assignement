package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Versement extends Operation{
  ////il est utilise pour cerialiser ou decerialiser un objet
  public static final long serialVersionUID = 1L;


  @Column
  private String nomPrenomEmetteur;


  }
