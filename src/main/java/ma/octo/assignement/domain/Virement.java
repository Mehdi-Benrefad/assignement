package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Virement extends Operation{

  ////il est utilise pour cerialiser ou decerialiser un objet
  public static final long serialVersionUID = 1L;

  @ManyToOne
  private Compte compteEmetteur;


}
